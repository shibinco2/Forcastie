package cusat.rissv.forecastie.tasks;

public enum ParseResult {OK, JSON_EXCEPTION, CITY_NOT_FOUND}